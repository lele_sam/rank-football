

  var module =  angular.module("MyApp", [])
module.controller('Controller', ['$scope', function ($scope) {
  
}])
module.directive('headerTemplate', function () {
  return {
    templateUrl: "views/header.html"
  };
})
module.directive('metodologia', function () {
  return {
    templateUrl: "views/metodologia.html"
  };
})
   


fetch('rank.json').then(response => response.json())
  .then(dati => {

    let sorted = dati.sort((a, b) => b.score - a.score)
    document.querySelector("#winner").innerHTML = sorted[0].team.toUpperCase()
    document.querySelector("#winnerScore").innerHTML = sorted[0].score
    document.querySelector("#winnerDesc").innerHTML = sorted[0].description


    var ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['I Trimeste', 'II Trimeste', 'III trimestre', 'IV Trimestre'],
        datasets: [{
          label: sorted[0].team.toUpperCase(),
          data: [86, 84, sorted[0].PrevScore, sorted[0].score,],
          backgroundColor: "rgba(255, 255 , 255 ,0.5)"

        },
        {
          label: 'Media Serie A',
          data: [74, 69, 70, 68,],
          backgroundColor: "rgba(0, 0 , 255 ,0.5)"

        }]

      }
    });


    let bigClubs = document.querySelector("#BigClubs")
    let BigClubsArray = []

    sorted.forEach(el => {
      if (el.team == "AC Milan" || el.team == "FC Internazionale" || el.team == "Juventus FC") {
        BigClubsArray.push(el)
      }

    });

    BigClubsArray.forEach(el => {
      let p = document.createElement('p')
      p.innerHTML =
        `<p  class="display-lg-3 h1 m-lg-2" style="margin-top:20px !important;"><img src="./media/${el.team}Logo.png" class="img-fluid" style="height:80px; filter:invert(1);"><strong class="text-uppercase "> ${el.team}</strong><span> ${el.score}</span></p>`
      bigClubs.appendChild(p)


    })

    var ctx = document.getElementById('ChartBigClubs').getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['I Trimeste', 'II Trimeste', 'III trimestre', 'IV Trimestre'],
        datasets: [{
          label: BigClubsArray[0].team,
          data: [86, 84, BigClubsArray[0].PrevScore, BigClubsArray[0].score,],
          backgroundColor: (BigClubsArray[0].team === "AC Milan") ? "rgba(255,0,0, 0.3)" : (BigClubsArray[0].team === "FC Internazionale") ? "rgba(0,0,255, 0.3)" : "rgba(255,255,255, 0.5)"

        },
        {
          label: BigClubsArray[1].team,
          data: [86, 84, BigClubsArray[1].PrevScore, BigClubsArray[1].score,],
          backgroundColor: (BigClubsArray[1].team === "AC Milan") ? "rgba(255,0,0, 0.3)" : (BigClubsArray[1].team === "FC Internazionale") ? "rgba(0,0,255, 0.3)" : "rgba(255,255,255, 0.5)"
        },
        {
          label: BigClubsArray[2].team,
          data: [86, 84, BigClubsArray[2].PrevScore, BigClubsArray[2].score,],
          backgroundColor: (BigClubsArray[2].team === "AC Milan") ? "rgba(255,0,0, 0.3)" : (BigClubsArray[2].team === "FC Internazionale") ? "rgba(0,0,255, 0.3)" : "rgba(255,255,255, 0.5)"
        },
        {
          label: 'Media Serie A',
          data: [74, 69, 70, 68,],
          backgroundColor: "#f9f9"

        }]

      }
    });


    let Top20 = document.querySelector("#top20")
    let Top20Array = []
 
    

    for (let i = 0; i < 10; i++) {
      Top20Array.push(sorted[i]) 
      
    }
    let position = 0
    Top20Array.forEach(el => {
      position = position +1
      let div = document.createElement('div')
      div.classList.add('col-lg-2', 'col-6', 'm-lg-2', 'my-lg-5',)
     
        div.innerHTML =
        ` <a id="modalLink" data-bs-toggle="modal" data-bs-target="#${el.team.split(" ").join("")}">
        <img src="media/${el.team}Logo.png" height="100"><br>
        <strong class="h2">${position} ${el.team}</strong><br>
        <span class="h3">${el.score}</span>
        </a>   

        <div class="modal fade" id="${el.team.split(" ").join("")}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-fullscreen">
      <div class="modal-content">
        <div class="modal-body" style="background: url('media/milan.png');">
        <button type="button" class="btn-close float-end" data-bs-dismiss="modal" aria-label="Close"></button>

        <div class="row align-items-center" style="position: relative;">
        <p  class=" h1  m-lg-2">${el.team}</p>
        <div class="col-12 col-lg-6  text-center">
        </div>
      </div>
        </div>
      </div>
    </div>
  </div> 
        `
      
       Top20.appendChild(div)



    })
  



  })

